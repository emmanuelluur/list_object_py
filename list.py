#!/usr/bin/env python
# coding: utf-8

# In[95]:


class List:
    cc = 0 # set counter
    def __init__(self):
        # each object instance increment the counter
        List.cc += 1 
    def SetList(self, ls):
        # create list
        self.list = ls;
    def AddItem(self, item):
        # add items
        (self.list).append({ "id": List.cc, "item": item })
    def ShowList(self):
        # #
        return self.list
    


# In[96]:


# 1st item
lista = []
libs1 = List()
libs1.SetList(lista)
libs1.AddItem('Lost Symbol')


# In[97]:


# 2nd item
libs2 = List()
libs2.SetList(lista)
libs2.AddItem('The Davinci Code')
total = libs2.ShowList()


# In[98]:


# list results
for book in total:
    print(f"ID {book['id']} Book {book['item']}")


# In[ ]:




